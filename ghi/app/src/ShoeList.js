import { useEffect, useState } from "react";
function ShoeList(props) {
    const [shoes, setShoes] = useState([])
    const getData = async () => {
        const response = await fetch('http://localhost:8080/api/shoes/');
        if (response.ok) {
            const data = await response.json();
            setShoes(data.shoes);
        }
    }
    useEffect(()=>{
        getData()
    }, [])
    
    const deleteShoes = async(id) => {
        const fetchConfig = {
            method: "DELETE",
            "Content-Type": "application/json",
        }
        const response = await fetch(`http://localhost:8080/api/shoes/${id}/`, fetchConfig)
        if (response.ok) {
            getData()
        }
    }

    return (
        <div className="container">
            <table className="table table-striped table-hover">
                <thead>
                    <tr>
                        <th> Picture</th>
                        <th>Model Name</th>
                        <th>Manufacturer</th>
                        <th>Color</th>
                        <th>Bin</th>
                    </tr>
                </thead>
                <tbody>
                    {props.shoes.map(shoe => {
                        console.log(shoe)
                        return (
                            <tr key={shoe.id}>
                                <td>
                                    <img src={ shoe.picture_url } height={200} className="photo .img-thumbnail" />
                                </td>
                                <td>{ shoe.model_name}</td>
                                <td>{ shoe.manufacturer }</td>
                                <td>{ shoe.color }</td>
                                <td>{ shoe.bin }</td>
                                <td>
                                    <button type="button" id={shoe.id} onClick={() => deleteShoes(shoe.id)} className="btn btn-danger">
                                        Delete
                                    </button>
                                </td>
                            </tr>
                        );
                    })};
                </tbody>
            </table>
        </div>
    )
}
export default ShoeList;
