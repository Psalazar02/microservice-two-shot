import React, { useEffect, useState } from 'react';

function CreateHatForm() {
    const [styleName, setStyleName] = useState("");
    const [fabric, setFabric] = useState("");
    const [color, setColor] = useState("");
    const [pictureUrl, setPictureUrl] = useState("");
    const [locations, setLocations] = useState([]);
    const [location, setLocation] = useState("");

    const handleStyleNameChange = (e) => {
        const value = e.target.value;
        setStyleName(value);
    };
    const handleFabricChange = (e) => {
        const value = e.target.value;
        setFabric(value);
    };
    const handleColorChange = (e) => {
        const value = e.target.value;
        setColor(value);
    };
    const handlePictureUrlChange = (e) => {
        const value = e.target.value;
        setPictureUrl(value);
    };
    const handleLocationChange = (e) => {
        const value = e.target.value;
        setLocation(value);
    };
    const handleSubmit = async (e) => {
        e.preventDefault();

        const data = {};
        data.style_name = styleName;
        data.fabric = fabric;
        data.color = color;
        data.picture_url = pictureUrl;
        data.location = location;

        const hatUrl = 'http://localhost:8090/api/hats/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(hatUrl, fetchConfig);
        if (response.ok) {
            const newHat = await response.json();

            setStyleName('');
            setFabric('');
            setColor('');
            setPictureUrl('');
            setLocation('');
        };
    };

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/locations/';
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setLocations(data.locations);
        };
    };

    useEffect(() => {
        fetchData();
    }, []);

return (
    <>
    <div className='row'>
    <div className='offset-3 col-6'>
            <div className='shadow p-4 mt-4'>
            <h1>Create a new hat</h1>
            <form onSubmit={handleSubmit} id="create-hat-form">
            <div className="form-floating mb-3">
                <input value={styleName}
                onChange={handleStyleNameChange}
                placeholder="Style Name"
                required type="text"
                name="style-name"
                id="style-name"
                className="form-control"/>
                <label htmlFor="style-name">Style Name</label>
            </div>
            <div className="form-floating mb-3">
                <input value={fabric}
                onChange={handleFabricChange}
                placeholder="Fabric"
                required type="text"
                name="fabric"
                id="fabric"
                className="form-control"/>
                <label htmlFor="fabric">Fabric</label>
            </div>
            <div className="form-floating mb-3">
                <input value={color}
                onChange={handleColorChange}
                placeholder="Color"
                required type="text"
                name="color"
                id="color"
                className="form-control"/>
                <label htmlFor="color">Color</label>
            </div>
            <div className="form-floating mb-3">
                <input value={pictureUrl}
                onChange={handlePictureUrlChange}
                placeholder="Picture Url"
                required type="text"
                name="picture"
                id="picture"
                className="form-control"/>
                <label htmlFor="picture-url">Picture Url</label>
            </div>
            <div className='mb-3'>
            <select value={location}
            onChange={handleLocationChange}
            required name="location"
            id="location"
            className="form-select">
                    <option>Choose a location</option>
                    {locations.map(location => {
                        return (
                            <option key={location.href} value={location.href}>
                                {location.closet_name}
                            </option>
                        );
                    })};
            </select>
            </div>
            <button className='btn btn-primary'>Create</button>
            </form>
            </div>
    </div>
    </div>
    </>
  );
}
export default CreateHatForm;
