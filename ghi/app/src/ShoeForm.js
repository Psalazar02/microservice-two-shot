import React, {useEffect, useState } from "react";

function ShoeForm(props) {
    const [modelName, setModelName] = useState("");
    const [manufacturer, setManufacturer] = useState("");
    const [color, setColor] = useState("");
    const [pictureUrl, setPictureUrl] = useState("");
    const [bin, setBin] = useState("");
    const [bins, setBins] = useState([]);

    const handleModelNameChange = (e) => {
        const value = e.target.value;
        setModelName(value);
    };
    const handleManufacturerChange = (e) => {
        const value = e.target.value;
        setManufacturer(value);
    };
    const handleColorChange = (e) => {
        const value = e.target.value;
        setColor(value);
    };
    const handlePictureUrlChange = (e) => {
        const value = e.target.value;
        setPictureUrl(value);
    };
    const handleBinChange = (e) => {
        const value = e.target.value;
        setBin(value);
    };
    const handleSubmit = async (e) => {
        e.preventDefault();

        const data = {};
        data.model_name = modelName;
        data.manufacturer = manufacturer;
        data.color = color;
        data.picture_url = pictureUrl;
        data.bin = bin;
        console.log(data)

        const shoeUrl = 'http://localhost:8080/api/shoes/';

        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(shoeUrl, fetchConfig);
        console.log(response)
        if (response.ok) {
            const newShoe = await response.json();

            setModelName('');
            setManufacturer('');
            setColor('');
            setPictureUrl('');
            setBin('');
        };
    };

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/bins/';
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setBins(data.bins);
        };
    };

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <>
        <div className='row'>
        <div className='offset-3 col-6'>
                <div className='shadow p-4 mt-4'>
                <h1>Add a shoe</h1>
                <form onSubmit={handleSubmit} id="create-shoe-form">
                <div className='form-floating mb-3'>
                    <input value={modelName} onChange={handleModelNameChange} placeholder='Model Name' required type='text' name='model_name' id='model_name' className='form-control'/>
                    <label htmlFor='modelName'>Model Name</label>
                </div>
                <div className="form-floating mb-3">
                    <input value={manufacturer} onChange={handleManufacturerChange} placeholder="Manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control"/>
                    <label htmlFor="manufacturer">Manufacturer</label>
                </div>
                <div className="form-floating mb-3">
                    <input value={color} onChange={handleColorChange} placeholder="Color" required type="text" name="color" id="color" className="form-control"/>
                    <label htmlFor="color">Color</label>
                </div>
                <div className="form-floating mb-3">
                    <input value={pictureUrl} onChange={handlePictureUrlChange} placeholder="Picture Url" required type="url" name="picture_url" id="picture_url" className="form-control"/>
                    <label htmlFor="pictureUrl">Picture Url</label>
                </div>
                <div className='mb-3'>
                <select value={bin} onChange={handleBinChange} required name="bin" id="bin" className="form-select">
                        <option>Choose a bin</option>
                        {bins.map(bin => {
                            return (
                                <option key={bin.href} value={bin.href}>
                                    {bin.closet_name}
                                </option>
                            );
                        })};
                </select>
                </div>
                <button className='btn btn-primary'>Create</button>
                </form>
                </div>
        </div>
        </div>
        </>
    )
};
export default ShoeForm;
