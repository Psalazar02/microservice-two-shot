import { useEffect, useState } from 'react';

function HatsList(props) {
    const [hats, setHats] = useState([])

    const getData = async () => {
        const response = await fetch('http://localhost:8090/api/hats/');

        if (response.ok) {
            const data = await response.json();
            setHats(data.hats);
        }
    }

    useEffect(()=>{
        getData()
    }, [])

    const deleteHats = async(id) => {
        const fetchConfig = {
            method: "DELETE",
            "Content-Type": "application/json",
        }
        const response = await fetch(`http://localhost:8090/api/hats/${id}/`, fetchConfig)
        if (response.ok) {
            getData()
        }
    }


    return (
        <table className="table table-striped">
            <thead>
            <tr>
                <th>Picture</th>
                <th>Style Name</th>
                <th>Fabric</th>
                <th>Color</th>
                <th>Location</th>
            </tr>
            </thead>
            <tbody>
            {props.hats.map(hat => {
                return (
                <tr key={hat.href}>
                    <td>
                        <img src={ hat.picture_url } height={200} className="photo .img-thumbnail" />
                    </td>
                    <td>{ hat.style_name }</td>
                    <td>{ hat.fabric }</td>
                    <td>{ hat.color }</td>
                    <td>{ hat.location }</td>
                    <td>
                        <button type="button" id={hat.id} onClick={() => deleteHats(hat.id)} className="btn btn-danger">
                            Delete
                        </button>
                    </td>
                </tr>
                );
            })}
            </tbody>
        </table>
    )
}
export default HatsList;
